<?php
/**
 * Template Name:  Frontpage
 *
 */
?>
<?php get_header(); ?>
<!-- Start of sections 1 -->
	<div class="hsections"> 
		<div class="hscroller">
			<section class="panel video_slider" id="video_height">
			<div class="fullcols">				
				<video width="1920" height="1080" class="video" autoplay="" loop="" muted="" controls="">
					<source src="<?php echo get_template_directory_uri(); ?>/images/homevideo.mp4" />
				</video>
			</div>
			</section>
		</div>
	</div>
<!-- End of sections 1 -->		
				
<!-- Start of sections 2 -->
	<div class="hsections"> 
		<div class="hscroller">	
			<section class="panel luxury_homes">
				<div class="fullcols">
					<div class="container-fluid py-2 py-sm-0">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 float-left">	
								<div class="animatedParent" data-sequence='500' data-appear-top-offset='200'><h1 class="col-12 col-sm-12 col-md-12 float-left text-left fs-xs-25 fs-lg-40 fs-xl-60 PrataRegular colorblack animated growIn slowest delay-250" data-id="1">A luxury address <span>you can call home</span></h1></div>
								<div class="animatedParent" data-sequence='500' data-appear-top-offset='200'><p class="col-12 col-sm-12 col-md-12 float-left text-left fs-xs-12 fs-lg-16 fs-xl-18 pt-3 RobotoRegular ttt animated growIn slowest delay-750" data-id="1">Home is where the heart is. We, at Baashyaam, believe in integrating the convenience of high-end amenities and world-class facilities in modern living to alter the lifestyle of urban families.</p></div>
							</div>
							<div class="col-12 col-sm-12 col-md-8 col-lg-8 float-left my-5 animatedParent" data-sequence='500' data-appear-top-offset='200'>
								<div class="animated fadeInDown slowest delay-500" data-id='2'>
									<img src="<?php echo get_template_directory_uri(); ?>/images/luxuryhomes.jpg" class="img-fluid float-md-right largeDP" alt="luxuryhomes" width="833" height="500" />
									<img src="<?php echo get_template_directory_uri(); ?>/images/luxuryhomes1080dp.jpg" class="img-fluid float-md-right bigDP" alt="luxuryhomes" width="1216" height="733" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
<!-- End of sections 2 -->

<!-- Start of sections 3 -->
	<div class="hsections">
		<div class="hscroller">			
			<section class="panel home_projects">
				<div class="fullcols">
					<div class="swiper-container homeprojcts">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<img src="<?php echo get_template_directory_uri(); ?>/images/Bonventura.jpg" class="" width="1450" height="630" alt="Projects"/>
								<div class="banner_captions">
									<div class="bc_holder">
										<span class="bigtitle ttt fs-xs-25 fs-lg-40 fs-xl-60 pb-0 mb-0 PrataRegular colorblack">Bonaventura</span><span class="smalltitle ttt RobotoRegular fs-xs-12 fs-lg-16 fs-xl-18">3 BHK Spanished styled homes</br>at <strong>RA Puram</strong></span>	
									</div>
									<h2 class="col-12 col-sm-12 col-md-12 float-left text-left p-0" data-id="3"><span class="highlight p-0 mt-2"><span><a href="#" class="fs-xs-12 fs-lg-16 fs-xl-18 RobotoBold">EXPLORE</a></span></span></h2>
								</div>
							</div>
							<div class="swiper-slide">
								<img src="<?php echo get_template_directory_uri(); ?>/images/Le-chalet.jpg" class="i" width="1450" height="630" alt="Projects"/>
								<div class="banner_captions">
									<div class="bc_holder">
										<span class="bigtitle ttt fs-xs-25 fs-lg-40 fs-xl-60 pb-0 mb-0 PrataRegular colorblack">Le Chalet</span><span class="smalltitle ttt RobotoRegular fs-xs-12 fs-lg-16 fs-xl-18">4 BHK Spanished styled homes</br>at <strong>RA Puram</strong></span>	
									</div>
									<h2 class="col-12 col-sm-12 col-md-12 float-left text-left p-0" data-id="3"><span class="highlight p-0 mt-2"><span><a href="#" class="fs-xs-12 fs-lg-16 fs-xl-18 RobotoBold">EXPLORE</a></span></span></h2>
								</div>
							</div>							
							<div class="swiper-slide">
								<img src="<?php echo get_template_directory_uri(); ?>/images/Plutus.jpg" class="" width="1450" height="630" alt="Projects"/>
								<div class="banner_captions">
									<div class="bc_holder">
										<span class="bigtitle ttt fs-xs-25 fs-lg-40 fs-xl-60 pb-0 mb-0 PrataRegular colorblack">Plutus</span><span class="smalltitle ttt RobotoRegular fs-xs-12 fs-lg-16 fs-xl-18">3 BHK Spanished styled homes</br>at <strong>RA Puram</strong></span>	
									</div>
									<h2 class="col-12 col-sm-12 col-md-12 float-left text-left p-0" data-id="3"><span class="highlight p-0 mt-2"><span><a href="#" class="fs-xs-12 fs-lg-16 fs-xl-18 RobotoBold">EXPLORE</a></span></span></h2>
								</div>
							</div>							
						</div>
						<!-- Add Pagination -->
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</section>
		</div>
	</div>
<!-- End of sections 3 -->

<!-- Start of sections 4 -->       
	<div class="hsections">
		<div class="hscroller">
			<section class="panel interiors_design">
				<div class="fullcols">
					<div class="container-fluid py-2 py-sm-0 px-0">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 float-left">
								<div class="animatedParent" data-sequence="800" data-appear-top-offset='200'>
									<h1 class="col-12 col-sm-12 col-md-12 float-left text-left fs-xs-25 fs-lg-40 fs-xl-60 pb-0 mpb-0 PrataRegular colorblack animated slower fadeInDown delay-250" data-id="1">Styled for luxury.</h1>
									<h1 class="col-12 col-sm-12 col-md-12 float-left text-left fs-xs-25 fs-lg-40 fs-xl-60 pt-0 mt-0 PrataRegular colorblack animated slower fadeInDown delay-250" data-id="2">Designed for<span>comfort.</span></h1>
									<p class="col-12 col-sm-12 col-md-12 float-left text-left py-3 RobotoRegular ttt fs-xs-12 fs-lg-16 fs-xl-18 animated slower fadeInDown delay-250" data-id="3">Carefully-crafted homes with appealing exteriors and heart-warming interiors. Each dwelling unit is designed for convenience, offering you a piece of heaven in the most sought-after residential localities of the city.</p>
									<h2 class="col-12 col-sm-12 col-md-12 float-left text-left py-0 animated slower fadeInDown delay-250" data-id="4"><span class="highlight p-0 m-0"><span><a href="#" class="fs-xs-12 fs-lg-16 fs-xl-18 RobotoBold">RESIDENTIAL PROJECTS</a></span></span></h2>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-8 col-lg-8 float-left py-2 py-sm-0">
								<div class="col-12 col-sm-12 col-md-12 p-0 float-left interidesign_gallery">
									<div class="col-12 col-sm-12 col-md-6 float-left text-center text-md-right text-lg-right p-0 px-md-3 interior_galleryleft">	
										<div class="animatedParent" data-sequence="700" data-appear-top-offset='300'>	
											<img src="<?php echo get_template_directory_uri(); ?>/images/interiors_design-leftfull-1080dp.jpg" class="img-fluid animated fadeInRight slowest delay-750" data-id="2" alt="interiors" width="406" height="527" />
										</div>
									</div>
									<div class="col-12 col-sm-12 col-md-6 float-left interior_galleryright px-0 pt-md-4 m-0 mt-lg-0 pr-lg-0 mr-0">
										<div class="animatedParent" data-sequence="700" data-appear-top-offset='300'>
											<img src="<?php echo get_template_directory_uri(); ?>/images/interiors_design-topright-1080dp.jpg" class="img-fluid pb-md-3 pb-lg-3 animated slowest fadeInDown delay-500 float-left float-md-right float-lg-right" data-id="2" alt="interiors" width="529" height="334" />
											<img src="<?php echo get_template_directory_uri(); ?>/images/interiors_design-bottomright-1080dp.jpg" class="img-fluid pb-md-3 pb-lg-3 animated slowest fadeInUp delay-500 float-left float-md-right float-lg-right" data-id="3" alt="interiors" width="529" height="334" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
<!-- End of sections 4 -->		
				
<!-- Start of sections 5 -->   		
	<div class="hsections">
		<div class="hscroller">	
			<section class="panel lifestyle_amenities">
				<div class="fullcols">
					<div class="container-fluid py-2 py-sm-0">
						<div class="row">
							<div class="lifestyles_captions">
								<div class="col-12 col-sm-12 col-md-4 col-lg-4 p-0 m-0 float-left">
								<div class="animatedParent" data-sequence="800" data-appear-top-offset='200'>
									<h1 class="col-12 col-sm-12 col-md-12 float-left text-left fs-xs-25 fs-lg-40 fs-xl-60 pt-0 mt-0 PrataRegular animated slower fadeInDown delay-250" data-id="1">Lifestyle<span>Amenities</span></h1>
									<p class="col-12 col-sm-12 col-md-12 float-left text-left py-3 RobotoRegular ttt fs-xs-12 fs-lg-16 fs-xl-18 animated slower fadeInDown delay-250" data-id="2">Make every day a luxurious one by indulging in state-of-the-art amenities that cater to the needs of every member in the family.</p>
									<h2 class="col-12 col-sm-12 col-md-12 float-left text-left py-0 animated slower fadeInDown delay-250" data-id="3"><span class="highlight p-0 m-0"><span><a href="#" class="fs-xs-12 fs-lg-16 fs-xl-18 RobotoBold">EXPLORE</a></span></span></h2>
								</div>
								</div>
							</div>
							
							<div class="lifestyles_gallery animatedParent" data-sequence="500" data-appear-top-offset='200'>
								<div class="animated fadeInDown slowest delay-750" data-id="3">
									<img src="<?php echo get_template_directory_uri(); ?>/images/Lifestyle.jpg" class="img-fluid lgcolheight" alt="Lifestyle" width="1500" height="745" />
								</div>
							</div>
						
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
<!-- End of sections 5 -->

<!-- Start of sections 6 -->        
	<div class="hsections">
		<div class="hscroller">
			<section class="panel about_baashyam">
			<div class="fullcols">
				<div class="container-fluid py-2 py-sm-0 pr-md-0">
					<div class="row px-0 mx-0">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6 float-left animatedParent px-0" data-sequence="500">
						<div class="animatedParent" data-sequence="800" data-appear-top-offset='200'>
							<h1 class="col-12 col-sm-12 col-md-12 float-left text-left fs-xs-25 fs-lg-40 fs-xl-60 px-0 PrataRegular colorblack animated slower fadeInRight delay-250" data-id="1">About Baashyam</h1>
							<p class="col-12 col-sm-12 col-md-12 float-left text-left fs-xs-12 fs-lg-16 fs-xl-18 px-0 RobotoRegular py-3 ttt animated slower fadeInRight delay-250" data-id="2">With top-notch properties developed in the most desired residential and commercial neighbourhoods, we are one of the nation's reputed luxury home builders.</p>
							<h2 class="col-12 col-sm-12 col-md-12 float-left text-left p-0 animated slower fadeInRight delay-250" data-id="3"><span class="highlight p-0 m-0"><span><a href="#" class="fs-xs-12 fs-lg-16 fs-xl-18 RobotoBold">EXPLORE</a></span></span></h2>							
						</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6 float-left text-center text-sm-center text-md-right  p-0">
							<div class="animatedParent" data-sequence="500" data-appear-top-offset='200'>
								<img src="<?php echo get_template_directory_uri(); ?>/images/about-baashyam.jpg" class="img-fluid float-md-right text-center text-sm-center text-md-right largeDP animated fadeInRight slowest delay-750" data-id="3" alt="luxuryhomes" width="424" height="700"/>
								<img src="<?php echo get_template_directory_uri(); ?>/images/about-baashyam-1080dp.jpg" class="img-fluid float-left float-md-right bigDP animated fadeInRight slowest delay-750" data-id="3" alt="luxuryhomes" width="545" height="900"/>
							</div>			
						</div>			
					</div>
					
				</div> 
				<div class="container-fluid py-2 py-sm-0">
					<div class="row px-0 mx-0">
					<div class="numeric_metrics animatedParent" id="foometrics">
						<div class="fullcols animatedParent" data-sequence="800" data-appear-top-offset='200'>
							<div class="landmarkers">
								<span class="counters PrataRegular colorblack fs-xs-30 fs-lg-45 fs-xl-56 animated slower growIn delay-250" data-id="2"><span id="counter"></span></span>
								<span class="ncaptions WorkSansRegular colorblack fs-xs-13 fs-lg-15 fs-xl-18  animated slower growIn delay-250" data-id="3">Landmarks created</span>
							</div>
							<div class="lux_sqft">
								<span class="counters PrataRegular colorblack fs-xs-30 fs-lg-45 fs-xl-56 colorblack animated slower growIn delay-250" data-id="2"><span id="counterlux"></span></span>
								<span class="ncaptions WorkSansRegular colorblack fs-xs-13 fs-lg-15 fs-xl-18 colorblack  animated slower growIn delay-250" data-id="3">sqft of luxury designed</span>
							</div>
							<div class="hpy_customers">
								<span class="counters PrataRegular colorblack fs-xs-30 fs-lg-45 fs-xl-56 colorblack animated slower growIn delay-250" data-id="2"><span id="counterhappy"></span></span>
								<span class="ncaptions WorkSansRegular colorblack fs-xs-13 fs-lg-15 fs-xl-18 colorblack   animated slower growIn delay-250" data-id="3">Happy Customers</span>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
<!-- End of sections 6 -->
		
<!-- Start of sections 7 -->        
	<div class="hsections">
		<div class="hscroller">
			<section class="panel footer_holder">
			<div class="fullcols">
				<div class="container-fluid py-2 py-sm-0 px-md-0 px-lg-0">
					<div class="row px-0 mx-0 pb-0 pb-md-5 pb-lg-5">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 float-left px-md-5 px-lg-5 px-xl-5 animatedParent" data-sequence="800" data-appear-top-offset='200'>
							<div class="fullcols footer_menus animated fadeInRight slowest delay-500" data-id="1">
								<h2><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoBold white">ONGOING PROJECTS</a></h2>
								<ul>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Bonaventura - RA Puram</a></li>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Pinnacle Crest - OMR</a></li>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Le Chalet - Thandalam</a></li>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Willow Square - Guindy</a></li>
								</ul>
							</div>	
							<div class="fullcols footer_menus animated fadeInRight slowest delay-500" data-id="2">
								<h2><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoBold white">ABOUT BAASHYAM</a></h2>
								<ul>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">About Us</a></li>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Media</a></li>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Awards</a></li>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Testimonials</a></li>
								</ul>
							</div>
							<div class="fullcols footer_menus animated fadeInRight slowest delay-500" data-id="3">
								<h2><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoBold white">CONTACT US</a></h2>
								<ul>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Connect</a></li>
									<li><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular ccc">Careers</a></li>
								</ul>
								<ul class="social_icons">
									<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/Facebook-india.png" class="img-fluid" alt="Facebook" width="25" height="32" /></a></li>										
									<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/Instagram-india.png" class="img-fluid" alt="Instagram" width="25" height="32" /></a></li>
									<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/Linkdin-india.png" class="img-fluid" alt="Linkdin" width="25" height="32" /></a></li>
								</ul>
							</div>
							<div class="fullcols footer_menus animated fadeInRight slowest delay-500" data-id="3">
								<h2><a href="" class="fs-xs-15 fs-lg-16 fs-xl-22 RobotoBold white">RECOGNIZED BY</a></h2>
								<ul>
									<li><img src="<?php echo get_template_directory_uri(); ?>/images/Credai-logo.png" class="img-fluid" alt="Facebook" width="198" height="31" /></a></li>
								</ul>
							</div>
							<div class="fullcols footer_menus animated fadeInRight slowest delay-500" data-id="4">
								<p class="col-12 col-sm-12 col-md-12 px-0 pt-4 pb-5 m-0 fs-xs-15 fs-lg-16 fs-xl-22 RobotoRegular white">© 2019, Baashyaam Constructions Pvt. Ltd. All Right Reserved</p>
							</div>
						</div>		
					</div>
				</div> 
			</div>
			</section>
		</div>
	</div>
<!-- End of sections 7 -->
<?php get_footer(); ?>