<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package baashyaam
 */

?>


	</div><!-- .horizontal_all -->
</div><!-- .horizontal_body -->

<?php wp_footer(); ?>
<?php if (is_front_page() || is_home()): ?>
<script type="text/javascript">

$(document).ready(function(){
//start of js for horizontal scrolling	
$get_width = $(window).width();
if($get_width > 768 )
{
	$target  = $(".horizontal_all").first(),
	currentPosition = parseInt($target.css("transform").split(",")[4]),
	moveBy = function(scrolledBy){
	currentPosition += scrolledBy;
	$target.css("transform", "translateX(" + (currentPosition) + "px)")
	},
	lastScrollTop = 0 ;

	currentPosition = isNaN(currentPosition) ? 0 : currentPosition;
	$(window).bind("scroll",function(e){
	var scrolledBy = $(window).scrollTop() - lastScrollTop;
	moveBy(-scrolledBy);
	lastScrollTop = $(window).scrollTop();
	}); 	
}


if($get_width > 768 )
{
	var swiper = new Swiper('.homeprojcts', {  mousewheel: false, direction: 'vertical', parallax: true,  speed: 1000, easing :500, effect: 'fade', slidesPerView: 1, autoplay : true, loop: true, keyboard: { enabled: false,}, pagination: { el: '.swiper-pagination', clickable: true,}, });
}
else
{
	var swiper = new Swiper('.homeprojcts', {  mousewheel: false, parallax: true,  speed: 1000, easing :500, effect: 'fade', slidesPerView: 1, autoplay : true, loop: true, keyboard: { enabled: false,}, pagination: { el: '.swiper-pagination', clickable: true,}, });
}

});
</script>
<?php endif; ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/css3-animate-it.js"></script>
</body>
</html>
