<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package baashyaam
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">	
	<?php wp_head(); ?>
	
	
</head>

<body <?php body_class(); ?> id="wheight">
<div class="header">
	<div class="row">
		<div class="container">
			<div class="logo_holder col-12 col-sm-12 col-md-6 float-left p-0">
				<a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/baashyamlogo.png" class="img-fluid" alt="Projects" width="154" height="27"/></a>
			</div>
			<div class="menus_holder col-12 col-sm-12 col-md-6 float-left p-0">
				<div id="cssmenu">
					<?php wp_nav_menu( array( 'container' => "cssmenu",) ); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="horizontal_body">
	<div class="horizontal_all">  
